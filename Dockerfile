FROM alpine

RUN apk update \
	&& apk add lighttpd \
	&& rm -rf /var/cache/apk/*

COPY ./lighttpd.conf /etc/lighttpd/lighttpd.conf

VOLUME /var/www/localhost/htdocs/

CMD ["lighttpd","-D","-f","/etc/lighttpd/lighttpd.conf"]
